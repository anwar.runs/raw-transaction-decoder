const fastify = require('fastify')({ logger: true });
const cors = require('@fastify/cors')
const txDecoder = require('ethereum-tx-decoder');

fastify.register(cors, {
})

function searchStringInArray(str, array) {
    for (let i = 0; i < array.length; i++) {
        if (array[i].includes(str.toLowerCase())) {
          return true;
        }
    }
    return false;
}

const contract = ["0x3b77e8849625E1C30dC16D046306178C1E0b45b1"];

function decodeRawTransaction(rawTx) {
    try {
        const decodedTx = txDecoder.decodeTx(rawTx);
        // Convert gasPrice, gasLimit, value to objects with _hex field
        const decodedTxWithHexObjects = {
            ...decodedTx,
            gasPrice: { _hex: decodedTx.gasPrice._hex },
            gasLimit: { _hex: decodedTx.gasLimit._hex },
            value: { _hex: decodedTx.value._hex }
        };
        return decodedTxWithHexObjects;
    } catch (error) {
        console.error('Error decoding transaction:', error);
        return null;
    }
}

fastify.post('/decode-raw-tx', async (request, reply) => {
    try {
        const { rawTransaction } = request.body;
        if (!rawTransaction) {
            throw new Error('Missing rawTransaction in request body');
        }

        const data = decodeRawTransaction(rawTransaction);
        if (data) {
            const found = searchStringInArray(data.to, contract);
            return reply.send({ success: true, data, freeGas: found });
        } else {
            return reply.status(400).send({ success: false, message: "Invalid rawTransaction." });
        }
    } catch (error) {
        reply.status(400).send({ success: false, error: error.message });
    }
});

// Run the server
const start = async () => {
    try {
        await fastify.listen({ port: 3000, host: "0.0.0.0"});
    } catch (err) {
        fastify.log.error(err);
        process.exit(1);
    }
};
start();
