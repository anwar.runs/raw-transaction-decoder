root@vmi1610272:~# vanityeth -i 0001
✔ {"account":{"address":"0x0001e1e263b4ac2efc73f7ab2e3c516b0a1c841b","privKey":"b461f7ec874e241e8c6ff79ed832d810cfbe98c3f472a4a90c88937475ddcd23"}}
echo "b461f7ec874e241e8c6ff79ed832d810cfbe98c3f472a4a90c88937475ddcd23" > pk_signer1
echo "jamsembilan50" > pw_signer1

root@vmi1610272:~# vanityeth -i 0002
✔ {"account":{"address":"0x0002be146604a32dffe6b47bee5b2d294243c98f","privKey":"3cf555b7a185df140447cf961e0ac8fe51fa62e3c8ff4ddf771eaf1d582ee57e"}}
echo "3cf555b7a185df140447cf961e0ac8fe51fa62e3c8ff4ddf771eaf1d582ee57e" > pk_signer2
echo "jamsembilan50" > pw_signer2

root@vmi1610272:~# vanityeth -i 0003
✔ {"account":{"address":"0x0003e535712fd743375f856a8cf309ee7252bef9","privKey":"f495eb892260aff0c2e398c09514e2d2c4a3f5c4f894a6764e7c51d59206384d"}}
echo "f495eb892260aff0c2e398c09514e2d2c4a3f5c4f894a6764e7c51d59206384d" > pk_signer3
echo "jamsembilan50" > pw_signer3

================

{
    "config": {
      "chainId": 41414,
      "homesteadBlock": 0,
      "eip150Block": 0,
      "eip155Block": 0,
      "eip158Block": 0,
      "byzantiumBlock": 0,
      "constantinopleBlock": 0,
      "petersburgBlock": 0,
      "istanbulBlock": 0,
      "berlinBlock": 0,
      "londonBlock": 0,
      "clique": {
        "period": 5,
        "epoch": 30000
      }
    },
    "nonce": "0x0",
    "timestamp": "0x0",
    "extraData": "0x00000000000000000000000000000000000000000000000000000000000000000001e1e263b4ac2efc73f7ab2e3c516b0a1c841b0002be146604a32dffe6b47bee5b2d294243c98f0003e535712fd743375f856a8cf309ee7252bef90000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
    "gasLimit": "0x59A5380",
    "difficulty": "0x1",
    "mixHash": "0x0000000000000000000000000000000000000000000000000000000000000000",
    "coinbase": "0x0000000000000000000000000000000000000000",
    "alloc": {
      "900c6f8AAcd4AA70F1477Be27CcbbD4bf9CC011E": {
          "balance": "10000000000000000000000000"
      },
      "73d004D298627140afcBe5F62A235ea188534742": {
          "balance": "10000000000000000000000000"
      },
      "75f685889516e51D3f2e7Db4633462e60563fA1b": {
          "balance": "10000000000000000000000000"
      }
    },
    "number": "0x0",
    "gasUsed": "0x0",
    "parentHash": "0x0000000000000000000000000000000000000000000000000000000000000000"
  }

geth account import --datadir=/root/testnet/signer1/data --password /root/testnet/signer1/pw_signer1 /root/testnet/signer1/pk_signer1
geth account import --datadir=/root/testnet/signer2/data --password /root/testnet/signer2/pw_signer2 /root/testnet/signer2/pk_signer2
geth account import --datadir=/root/testnet/signer3/data --password /root/testnet/signer3/pw_signer3 /root/testnet/signer3/pk_signer3

enode://42fb73a3bf0f56337e11d83637fe740dca3f95f3d28ae86cf772440cfa9947ba679416545e681a9462b7494a4a4de5f49e07e64d52c5f3b6a7ac97afe4668110@127.0.0.1:30303?discport=0

geth --nousb --datadir "/root/testnet/signer1/data" --http.corsdomain '*' --http.vhosts '*' --miner.etherbase=0x0001e1e263b4ac2efc73f7ab2e3c516b0a1c841b --mine --allow-insecure-unlock --unlock 0x0001e1e263b4ac2efc73f7ab2e3c516b0a1c841b --password /root/testnet/signer1/pw_signer1 --config /root/testnet/signer1/config.toml
geth --nousb --datadir "/root/testnet/signer2/data" --http.corsdomain '*' --http.vhosts '*' --miner.etherbase=0x0002be146604a32dffe6b47bee5b2d294243c98f --mine --allow-insecure-unlock --unlock 0x0002be146604a32dffe6b47bee5b2d294243c98f --password /root/testnet/signer2/pw_signer2 --config /root/testnet/signer2/config.toml
geth --nousb --datadir "/root/testnet/signer3/data" --http.corsdomain '*' --http.vhosts '*' --miner.etherbase=0x0003e535712fd743375f856a8cf309ee7252bef9 --mine --allow-insecure-unlock --unlock 0x0003e535712fd743375f856a8cf309ee7252bef9 --password /root/testnet/signer3/pw_signer3 --config /root/testnet/signer3/config.toml
geth --nousb --datadir "/root/testnet/bootnode/data" --http.corsdomain '*' --http.vhosts '*' --config /root/testnet/bootnode/config.toml 


===========

[Unit]
Description=Ethereum Signer 01
After=network.target

[Service]
ExecStart=geth --nousb --datadir "/root/testnet/signer1/data" --http.corsdomain '*' --http.vhosts '*' --miner.etherbase=0x0001e1e263b4ac2efc73f7ab2e3c516b0a1c841b --mine --allow-insecure-unlock --unlock 0x0001e1e263b4ac2efc73f7ab2e3c516b0a1c841b --password /root/testnet/signer1/pw_signer1 --config /root/testnet/signer1/config.toml
Restart=always
User=root
Group=root

[Install]
WantedBy=default.target

===========

[Unit]
Description=Ethereum Signer 02
After=network.target

[Service]
ExecStart=geth --nousb --datadir "/root/testnet/signer2/data" --http.corsdomain '*' --http.vhosts '*' --miner.etherbase=0x0002be146604a32dffe6b47bee5b2d294243c98f --mine --allow-insecure-unlock --unlock 0x0002be146604a32dffe6b47bee5b2d294243c98f --password /root/testnet/signer2/pw_signer2 --config /root/testnet/signer2/config.toml
Restart=always
User=root
Group=root

[Install]
WantedBy=default.target

===========

[Unit]
Description=Ethereum Signer 03
After=network.target

[Service]
ExecStart=geth --nousb --datadir "/root/testnet/signer3/data" --http.corsdomain '*' --http.vhosts '*' --miner.etherbase=0x0003e535712fd743375f856a8cf309ee7252bef9 --mine --allow-insecure-unlock --unlock 0x0003e535712fd743375f856a8cf309ee7252bef9 --password /root/testnet/signer3/pw_signer3 --config /root/testnet/signer3/config.toml
Restart=always
User=root
Group=root

[Install]
WantedBy=default.target 
